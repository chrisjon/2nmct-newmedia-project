// import com.onformative.leap.*;
import com.neurogami.leaphacking.*;
import com.leapmotion.leap.*;
import krister.Ess.*;
// import processing.core.*;
// import com.leapmotion.leap.CircleGesture;
// import com.leapmotion.leap.Gesture.State;
// import com.leapmotion.leap.Gesture.Type;
// import com.leapmotion.leap.Hand;
// import com.leapmotion.leap.KeyTapGesture;
// import com.leapmotion.leap.ScreenTapGesture;
// import com.leapmotion.leap.SwipeGesture;
// import com.onformative.leap.LeapMotionP5;
// import com.leapmotion.leap.GestureList.*;

//Declare global variables
static final int GAME_INIT = 0, GAME_START = 1, GAME_PAUZE = 2, GAME_OVER = 3, GAME_END = 4, GAME_HIGHSCORE = 5, GAME_SETTINGS = 6, GAME_RULES = 7;
static final int MENU_RESUME = 0, MENU_START = 1, MENU_HIGHSCORE = 2, MENU_STOP = 3, MENU_HOME = 4, MENU_RULES = 5;
static final int PLAY_FIRSTWALL = 0, PLAY_RUNNING = 1, PLAY_REPEATFIRST = 2, PLAY_REPEATSECOND = 3, PLAY_SPAWNORB = 4, PLAY_HIDEORB = 5;
static final int ORB_EMPTY = 0, ORB_BONUS = 1, ORB_HEALTH = 2, ORB_SHIELD = 3, ORB_SLOW = 4;
static final int[] orbColor = {0xFFFFFFF,0xAA006699,0xAAFF3366,0xAAAAAAAA,0xAA669900};
// Declare leap motion
LeapMotionP5 leap;
PointerListener listener   = new PointerListener();
Controller controller = new Controller(listener);
int gameState, playerSize = 50;
String lastGesture = "enabling gestures: \n'c' for CircleGesture\n's' for SwipeGesture\n'k' for KeyTapGesture\n't' for ScreenTapGesture";

MenuItem[] menuItems = new MenuItem[6]; //0, menuItem1, menuItem2, menuPauzeResume;
GameWall gwall1, gwall2;
Orb gBonus;
public BoundingBox boundingBox;
Vector avgPos = Vector.zero();
Vector normalizedAvgPos = Vector.zero();
    PVector translation = new PVector();
// Vector normalizedAvgPosScreen = Vector.zero();
float prevcM1000 = 0;
int playState = -1;
Highscore highscoreList; 
int lives, invincibilityCounter;
float score;
int twitterOpened = -1;
float c, multiplier;
float[] randomvalueX = new float[22], randomvalueY = new float[22];
PImage bg, title, gameBackground;
PShape player;
float speedModifier1, speedModifier2;
PShape[] starImages = new PShape[3];
PFont spacebarFont, cijfers;
PImage twitter;
String typing = "";
// Variable to store saved text when return is hit
String saved = "";
AudioChannel health, bonus, shield, slow, backgroundMusic; 
boolean pressedEnter = false;

public void setNormalizedAvgPos(Vector newNormalized) {
  normalizedAvgPos = newNormalized;
  // normalizedAvgPosScreen.setX(map(normalizedAvgPos.getX(),0.0,1.1)
    // normalizedAvgPosScreen.setX(map(newNormalized.getX(), 0.0,1.0,0.0, sketchWidth()));
    // normalizedAvgPosScreen.setY(map(newNormalized.getY(),0.0,1.0, sketchHeight(), 0.0));
    
    translation.x = map(normalizedAvgPos.getX(), 0.0,1.0,0.0, sketchWidth());
    translation.y = map(normalizedAvgPos.getY(),0.0,1.0, sketchHeight(), 0.0);
}

public void setNormalizedAvgPos(float x, float y) {
	normalizedAvgPos.setX(x);
	normalizedAvgPos.setY(y);
    translation.x = map(x, 0.0,1.0,0.0, sketchWidth());
    translation.y = map(y,0.0,1.0, 0.0, sketchHeight());
}

void setup() {
  // size(1280, 720, P3D);
  spacebarFont = loadFont("SPACEBAR-100.vlw");
  cijfers = loadFont("PlanetBensonTwo-Regular-100.vlw");
  textFont(spacebarFont);
  noFill();
  stroke(255);
  leap = new LeapMotionP5(this);
  // leap.
  gameState = GAME_INIT;

  menuItems[MENU_RESUME] = new MenuItem("Resume", 140, sketchWidth()/2, sketchHeight()/2, 0, sketchHeight()/2); //gamepauze
  menuItems[MENU_START] = new MenuItem("Start", 120);
  menuItems[MENU_HIGHSCORE] = new MenuItem("Highscores", 120);
  menuItems[MENU_STOP] = new MenuItem("Stop", 120);
  menuItems[MENU_HOME] = new MenuItem("Home", 120);
  menuItems[MENU_RULES] = new MenuItem("Rules", 120);
  highscoreList = new Highscore();
  for (int i=0;i<20;i++) {
  randomvalueX[i] = random(0, sketchWidth());
  randomvalueY[i] = random(0, sketchHeight());
  }
  // bg = loadImage("NGC_5584.jpg");
  player = loadShape("player.svg");
  title = loadImage("Title.png");
  gameBackground = loadImage("Background/hyperspaceeffect.jpg");
  gameBackground.resize(sketchWidth(),sketchHeight());
  starImages[0] = loadShape("Stars/Bellatrix.svg");
  starImages[1] = loadShape("Stars/Pollux.svg");
  starImages[2] = loadShape("Stars/Siruis.svg");
  
  twitter = loadImage("Twitter_logo_blue.png");
  twitter.resize(200, 163);
  Ess.start(this);
  health = new AudioChannel("Audio/Health.wav");
  bonus= new AudioChannel("Audio/BonusPoints.wav");
  shield= new AudioChannel("Audio/Shield.wav");
  slow= new AudioChannel("Audio/Slow.wav");
  backgroundMusic = new AudioChannel("Audio/bg.wav");
  backgroundMusic.loop(Ess.FOREVER);
  backgroundMusic.play();
}

void mouseDragged(){
  mouseMoved();
}

void mouseMoved(){
	setNormalizedAvgPos(mouseX*1.0/sketchWidth(), mouseY*1.0/sketchHeight());
	avgPos.setX(mouseX);
	avgPos.setY(mouseY);
	// println("mouseX: "+mouseX*1.0/sketchWidth());
	// println("mouseY: "+mouseY*1.0/sketchHeight());
}

public int sketchWidth(){
  return displayWidth;
}

public int sketchHeight() {
  return displayHeight;
}

public String sketchRenderer() {
  return P3D; 
}

void draw() {
  background(gameBackground);
  textFont(spacebarFont);
  // sphere(50);
  drawPlayer();

	// menuItem();
  // background(250);
  // for (Hand hand : leap.getHandList()) {
  //     pushMatrix();
  //     PVector handPosition = leap.getPosition(hand);
  //     translate(handPosition.x, handPosition.y, (handPosition.z/2)-50);

  //     float pitch = leap.getPitch(hand);
  //     float roll = leap.getRoll(hand);
  //     float yaw = leap.getYaw(hand);
  //     // rotateX(radians(map(pitch, -30, 30, 45, -45)));
  //     // rotateY(radians(map(yaw, -12, 18, 45, -45)));
  //     // rotateZ(radians(map(roll, -40, 40, 45, -45)));

  //     float handSize = leap.getSphereRadius(hand);
  //     println(handPosition);
  //     sphere(60);
  //     popMatrix();
  //   }
  switch (gameState) {
  	case GAME_INIT :
    pressedEnter = false;
      image(title, sketchWidth()/2-title.width/2, 10);
  		gameMenu();
  		break;
  	case GAME_START :
      gameBackGround();
      renderCurrentGameLevel();
      renderScoreAndLoves();
  		break;	
  	case GAME_PAUZE :
  		gamePauze();
  		break;
  	case GAME_OVER :
      gameOver();
  		break;
  	case GAME_END :
  		break;
  	case GAME_HIGHSCORE :
  	highScore();
  		break;
  	case GAME_SETTINGS :
  		break;	
    case GAME_RULES :
      renderHowToPlay();
      break;  
  }
  
}
void renderScoreAndLoves(){
  textFont(cijfers);
  textSize(40);
  fill(255);
  textAlign(LEFT);
  // String cLive = (invincibilityCounter>0) ? "∞": lives;
  text("Score: "+int(score), 150, sketchHeight()/1.2);
  if(invincibilityCounter>0)
    text("Lives: (shield) "+lives, 150, sketchHeight()/1.1);
  else
    text("Lives: "+lives, 150, sketchHeight()/1.1);
}
void renderHowToPlay(){
  menuItems[MENU_HOME].draw((sketchWidth()/1.1), sketchHeight()/2);
if(menuItems[MENU_HOME].onclickedMenuItem(translation))
  gameState = GAME_INIT;
  translate(sketchWidth()/2, sketchHeight()/2.2);
    rectMode(CENTER);
    fill(255,100);
    rect(0, 0, 700, 600, 7);
    textSize(80);
    text("How to play", 0,-310);
    textFont(cijfers);
    textSize(30);
    fill(0);
    translate(-300,-250,30);
    fill(orbColor[1]);
    stroke(orbColor[1]);
    sphere(30);
    fill(0);
    text("This sphere give you bonus points",320,0);
    translate(0, 85);
    fill(orbColor[2]);
    stroke(orbColor[2]);
    sphere(30);
    fill(0);
    text("This sphere give you one live",300,0);
    translate(0, 85);
    fill(orbColor[3]);
    stroke(orbColor[3]);
    sphere(30);
    fill(0);
    text("This sphere give you a shield",300,0);
    translate(0, 85);
    fill(orbColor[4]);
    stroke(orbColor[4]);
    sphere(30);
    fill(0);
    text("This will lower the game space",300,0);
    translate(0, 85);
    fill(0);
    text("How to play",300,0);
    textSize(25);
    fill(0);
    text("Avoid the stars by moving through", 300, 30);
    text("the black spots marked on your HUD,", 300, 60);
    text("otherwise your planet will get destroyed", 300, 90);
}
void gameOver(){
  // score = 15486;
  textSize(100);
  text("Game Over", sketchWidth()/2, sketchHeight()/6);
  textSize(48);
  text("Your score is:",sketchWidth()/2, sketchHeight()/4);
  textFont(cijfers);
  textSize(48);
  text(int(score), sketchWidth()/2, sketchHeight()/3.4);
  textFont(spacebarFont);
  textSize(20);
  text("Enter your name and hit enter", sketchWidth()/2, sketchHeight()/3);
  text(typing, sketchWidth()/2, sketchHeight()/2.6);
  if(saved != null && saved != ""){
    highscoreList.putHighscore(saved, int(score));
    saved = null;
  }
  image(twitter, sketchWidth()/2-100, sketchHeight()/2.5);
  // open("http://www.blabla.com");
  if(dist(translation.x, translation.y, (sketchWidth()/2), (sketchHeight()/2.5)+100) < 200){
    if(twitterOpened == 0){
      twitterOpened = 1;
      link("https://twitter.com/home?status=Awesome,%20I%20just%20scored%20"+int(score)+"%20on%20Save%20The%20Planet!%20http://stp.fluid.desi", "_new");
    }
  } else if(twitterOpened == -1)
    twitterOpened = 0;

  textSize(20);
  menuItems[MENU_HOME].draw((sketchWidth()/2), sketchHeight()/1.5);
if(menuItems[MENU_HOME].onclickedMenuItem(translation))
  gameState = GAME_INIT;
  

}

void keyReleased() {
  // If the return key is pressed, save the String and clear it
  println("key: "+key);
   if (key != CODED) {
    switch(key) {
    case BACKSPACE:
      typing = typing.substring(0,max(0,typing.length()-1));
      break;
    case TAB:
      typing += "    ";
      break;
    case ENTER:
    case RETURN:
      // comment out the following two lines to disable line-breaks
      //typedText += "\n";
      //break;
    case ESC:
    case DELETE:
      break;
    default:
      // typing += key;
    } 
  if (key == '\n' && gameState==GAME_OVER && !pressedEnter && typing!= "" && typing != null) {
    saved = typing;
    // A String can be cleared by setting it equal to ""
    typing = ""; 
    pressedEnter = true;
  }else if(key == 'p' && (gameState == GAME_START || gameState == GAME_PAUZE)){
    if(gameState==GAME_START)
      gameState = GAME_PAUZE;
    else
      gameState = GAME_START;
  } else
    // Otherwise, concatenate the String
    // Each character typed by the user is added to the end of the String variable.
    typing = typing + key; 
  }
}

void createStar(int aantal){
  for (int i =0; i<aantal;i++) {
    
  
  pushMatrix();
  fill(#ffd2a1);
  translate(randomvalueX[i],randomvalueY[i]);
  rotate(frameCount / -100.0);
  star(0, 0, 30/3, 70/3, 5); 
  popMatrix();
  }
}

void gameMenu(){
  pushMatrix();
  translate(0, 0, -200);
  // image(bg,0,0);
    createStar(20);
	  noFill();
	  stroke(255);
    // menuitems.forEa
    menuItems[MENU_START].draw((sketchWidth()/4),(sketchHeight()/1.5));
    if(menuItems[MENU_START].onclickedMenuItem(translation))
      initializeGame();
	  menuItems[MENU_STOP].draw((sketchWidth()/2),(sketchHeight()/1.6));
    if(menuItems[MENU_STOP].onclickedMenuItem(translation))
      exit();
  	menuItems[MENU_HIGHSCORE].draw((sketchWidth()/1.5),(sketchHeight()/1.3));
    if(menuItems[MENU_HIGHSCORE].onclickedMenuItem(translation))
      gameState = GAME_HIGHSCORE;
    menuItems[MENU_RULES].draw((sketchWidth()/1.5),(sketchHeight()/2));
    if(menuItems[MENU_RULES].onclickedMenuItem(translation))
      gameState = GAME_RULES;
  popMatrix();
  	
}
void star(float x, float y, float radius1, float radius2, int npoints) {
  float angle = TWO_PI / npoints;
  float halfAngle = angle/2.0;
  beginShape();
  for (float a = 0; a < TWO_PI; a += angle) {
    float sx = x + cos(a) * radius2;
    float sy = y + sin(a) * radius2;
    vertex(sx, sy);
    sx = x + cos(a+halfAngle) * radius1;
    sy = y + sin(a+halfAngle) * radius1;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}

void initializeGame() {
  score = 0;
  c = -500;
  multiplier = 5; //TODO: CHANGE back to 2
  gameState = GAME_START;
  gameBackGround();
  float width = sketchWidth();
  float height = sketchHeight();
  float offW = width/5;
  float offH = height/4;
  boundingBox = new BoundingBox(offW,offH,width-offW,height-offH);
  gwall1 = new GameWall(c, boundingBox);
  gwall2 = new GameWall(c*2,boundingBox);
  gBonus = new Orb(-750);
  lives = 3;
  twitterOpened = -1;
}

void gameBackGround(){
  //translate(x, y, z); //move object on tick
  pushMatrix();
  translate(0, 0, -500);
  // image(gameBackground, 0, 0, sketchWidth(), sketchHeight());
  popMatrix();
  pushMatrix();
  float width = sketchWidth();
  float height = sketchHeight();
  float offW = width/5;
  float offH = height/4;
  noFill();
  // textureMode(NORMAL);
  beginShape(QUADS);
  // texture(gameBackground);
  vertex(0, 0, 200,0,0);
  vertex(0, height, 200,1,0);
  vertex(offW, height-offH, -500,1,1);
  vertex(offW, offH, -500,0,1);
// endShape();
  vertex(offW, offH, -500);
  vertex(width-offW, offH, -500);
  vertex(width, 0, 200);
  vertex(0, 0, 200);
  
  vertex(width, height, 200);
  vertex(width, 0, 200);
  vertex(width-offW, offH, -500);
  vertex(width-offW, height-offH, -500);

  vertex(width-offW, height-offH, -500);
  vertex(offW, height-offH, -500);
  vertex(0, height, 200);
  vertex(width, height, 200);
  
  endShape();
  popMatrix();
}

void renderCurrentGameLevel() {
  float width = sketchWidth();
  float height = sketchHeight();
  float offW = width/5;
  float offH = height/4;

  float cM1000 = c % 1000;
  if(prevcM1000<500 && cM1000>499)
    playState = PLAY_REPEATFIRST;
  else if(prevcM1000>500 && cM1000<500)
    playState = PLAY_REPEATSECOND;
  else
    playState = PLAY_RUNNING;
  if (c==0) {
    playState = PLAY_FIRSTWALL;
  }
  prevcM1000 = cM1000;
  // int cM2000 = c % 2000;
  switch (playState) {
    case PLAY_REPEATSECOND:
      gwall2.toBack();
      multiplier*=(multiplier>7)?((multiplier>15)?((multiplier>20)?1.005:1.025):1.05):1.10;
      // println("multiplier: "+multiplier);
      println(multiplier + " PLAY_REPEATSECOND: "+ playState);
    case PLAY_FIRSTWALL:
      println("PLAY_FIRSTWALL: "+playState);
      println("cM1000: "+cM1000);
      gwall1.passThrough();
    break;
    case PLAY_REPEATFIRST:
      println("PLAY_REPEATFIRST: "+playState);
      println("cM1000: "+cM1000);
      gwall2.passThrough();
      gwall1.toBack();
    break;  
    // case 1000:
    //   speedModifier1 /= 2;
    //   speedModifier2 *= 2;
    // break;  
  }
    // if(cM1000 == 500) {
    //   speedModifier1 *= 2;
    // }
    // else if (cM2000 == 1000) {
    //   speedModifier2 *= 2;
    //   speedModifier1 /= 2;
    // }
    // else if (cM2000 == 1500) {
    //   speedModifier2 /= 2;
    //   speedModifier1 *= 2;
    // }
  //TODO: renderWall
  
    c+=multiplier;
    score += multiplier/10;
    text(score, offW, height-offH, -25);
  gwall1.draw(multiplier, width, height, offW, offH);
  if(gwall2 != null)
    gwall2.draw(multiplier, width, height, offW, offH);
  // bonus orb calculations
  gBonus.draw();
  if(invincibilityCounter>0)
    invincibilityCounter--;
  if(gBonus.inOrbVincinity(translation)){
    println("detected orb" + gBonus.orbType); //TODO
    switch(gBonus.orbType){
      case ORB_SLOW:
        multiplier/=(multiplier>7)?((multiplier>15)?((multiplier>20)?1.01:1.05):1.1):1.2;
        slow.play();
      break;
      case ORB_HEALTH:
        lives++;
        health.play();
      break;
      case ORB_BONUS:
        score += int(random(15,100));
        bonus.play();
      break;
      case ORB_SHIELD :
        invincibilityCounter = 100;
        shield.play();
      break;  
    }
  }
}

void drawPlayer() {
  // for (Hand hand : leap.getHandList()) {
    pushMatrix();
    textAlign(CENTER);
    // fill(255);
    // text("X: " + int(normalizedAvgPos.getX()*100)/100.0 + " & " + int(avgPos.getX()*100)/100.0,300,250,200);
    // text("Y: " + int(normalizedAvgPos.getY()*100)/100.0 + " & " + int(avgPos.getY()*100)/100.0,300,300,200);
    // text("Z: " + int(avgPos.getZ()*100)/100.0,300,350,200);
    // noFill();
    translate(translation.x, translation.y, translation.z);
    // float pitch = leap.getPitch(hand);
    // float roll = leap.getRoll(hand);
    // float yaw = leap.getYaw(hand);
    // rotateX(radians(map(pitch, -30, 30, 45, -45)));
    // rotateY(radians(map(yaw, -12, 18, 45, -45)));
    // rotateZ(radians(map(roll, -40, 40, 45, -45)));

    // float handSize = 50;//leap.getSphereRadius(hand);
    // text(avgPos.getZ(),50,0);
    // stroke(55,55,255);
    tint(255,150);
    // image(player, -50, -50, 100, 100);
    shape(player, 0, 0, 120, 120);
    // sphere(50);
    popMatrix();
    // ellipse(normalizedAvgPos.getX(), normalizedAvgPos.getY(), 100, 100);
    // line(normalizedAvgPos.x, normalizedAvgPos.y, normalizedAvgPos.x + velocity.x, normalizedAvgPos.y + velocity.y);
    // sphere(50);
  //   }
}

void gamePauze(){
  pushMatrix();
  translate(sketchWidth()/2,0);
	textSize(32);
	fill(255);
	text("Game pauzed", 0, sketchHeight()/15,20);
  noFill();
  stroke(255);
  menuItems[MENU_RESUME].draw();
  if(menuItems[MENU_RESUME].onclickedMenuItem(translation))
    gameState = GAME_START;
  menuItems[MENU_STOP].draw(sketchWidth()/4,sketchHeight()/2);
  if(menuItems[MENU_STOP].onclickedMenuItem(translation))
    gameState = GAME_INIT;
  menuItems[MENU_HIGHSCORE].draw(-sketchWidth()/4,sketchHeight()/2);
  if(menuItems[MENU_HIGHSCORE].onclickedMenuItem(translation))
    gameState = GAME_HIGHSCORE;
  popMatrix();	
}

void highScore(){
	
	highscoreList.draw();
}
