public class BoundingBox {
	float minX, minY, maxX, maxY;

	public BoundingBox (float minX, float minY, float maxX, float maxY) {
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
	}

	public BoundingBox(float maxX, float maxY) {
		this(0,0,maxX,maxY);
	}

}