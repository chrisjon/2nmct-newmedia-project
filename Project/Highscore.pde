	import processing.net.*;

	public class Highscore {
		
		static final String URL = "http://nl203.jlss.eu/api.php?api=get";
		static final String usernameJson = "name";
		static final String highscoresJson = "score";
		JSONArray values, v;

		public Highscore() {
			values = getHighscore();
		}

		public void putHighscore(String name, int score) {
			try {
				v = loadJSONArray("http://nl203.jlss.eu/api.php?api=set&name="+name+"&score="+score+"");
				values = getHighscore();
			}catch (Exception e) {
				
			}
		}

		public JSONArray getHighscore() {
			// json = loadJSONObject(URL);
			// String[] username = json.getString(usernameJson);
			// int[] highscores = json.getString(highscoresJson);
			values = new JSONArray();
			try {
				
			
			values = loadJSONArray(URL);
		}catch (Exception e) {
			println("Kan niet connecteren met de server");
			println("e: "+e);
		}
			return values;

			
	}

	public void draw(){
		// ervoor zorgen dat alle variabelen automatisch bepaalt worden
		pushMatrix();
		menuItems[MENU_HOME].draw((sketchWidth()/1.23), sketchHeight()/2);
		if(menuItems[MENU_HOME].onclickedMenuItem(translation))
  		gameState = GAME_INIT;
		translate(sketchWidth()/2, sketchHeight()/2);
		rectMode(CENTER);
		fill(255,100);
		rect(0, 0, 500, 500, 7);
		fill(0);
		pushMatrix();
		translate(0, -225);
		textFont(cijfers);
		textSize(20);
		textAlign(CENTER, TOP);
		text("Highscores",0,0);
		
		translate(-150, 50);
		text("Name", 15, -30);
		text("Score", 300, -30);
		//test
		for (int i = 0; i < values.size(); i++) {
	    
	    	JSONObject scoreObject = values.getJSONObject(i); 

	    	// int id = animal.getInt("id");
	    	String name;
	    	try{
	    		name = scoreObject.getString(usernameJson);
	    	} catch (Exception e) { //if no name, catch
	    		name = "Anonym";
	    	}
	    	String score = scoreObject.getString(highscoresJson);

	    	textAlign(LEFT);
	    	text(name, 0, 20+25*i);
	    	text("---------------------------"+score, 50, 20+25*i);
	  }

		//test


		
		popMatrix();
		popMatrix();
	}

	}

	