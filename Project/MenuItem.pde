public class MenuItem {
	int[] koekjes = {-2,-1,1,2};
	String menuText;
	int radius;
	float sphereX, sphereY, sphereZ, useX = 0, useY = 0;
	int zdir, zCount;
	int inRange = 0;
	PImage bg;

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public MenuItem (String menuText, int radius) {
		this.menuText = menuText;
		this.radius = radius;
		this.sphereZ = random(-150, 150);
		this.zdir = koekjes[int(random(koekjes.length))];
		bg = loadImage(menuText+".png");
	}

	public MenuItem (String menuText, int radius, float x, float y, float useX, float useY) {
		this(menuText,radius);
		sphereX = x;
		sphereY = y;
		this.useX = useX;
		this.useY = useY;
	}

	void draw(){
		draw(useX,useY);
	}

	void draw(float x, float y){
		textSize(30);
		sphereZ+=zdir;
		if(zCount>50) {
			zCount = 0;
			zdir*=-1;
		}
		zCount++;
		if(useX==0)
			sphereX = x;
		if(useY==0)
			sphereY= y;
		pushMatrix();
		translate(x, y, sphereZ);
		if(inRange>185) {
  			tint(255, 0);
		}
  		else
	  		tint(255, 255-int(inRange));
  		// sphere(radius);
  		if(menuText == "Start" || menuText == "Resume")
  			image(bg,-(radius/2),-(radius/2),radius,radius);
  		else
  			image(bg,-(radius/2),-(radius/2),radius,radius);
  		strokeWeight(1);
		stroke(0,0,0);
		fill(200,150);
		// line(-28,-25,-28,32);
		// line(-28,32,35,3);
		// line(35,3,-28,-25);
		if(menuText == "Start" || menuText == "Resume"){
			beginShape();
			vertex(-28,-25,-28,32);
			vertex(-28,32,35,3);
			vertex(35,3,-28,-25);
			endShape(CLOSE);
		}

  		textAlign(CENTER);
  		text(menuText, 0, 85, 20);
  		popMatrix();
	}

	public boolean onclickedMenuItem(PVector playerPos) {
		if(dist(playerPos.x, playerPos.y, sphereX, sphereY) <= radius){
			if(inRange<125) {
				inRange+=3;
				return false;
			} else {
				inRange = 0;
				return true;
			}
		}
		else {
    		// text("X: " + int(playerPos.x) + "Y: " + int(playerPos.y) + "Z: " + int(playerPos.z),300,100);
    		// text("X: " + int(sphereX) + "Y: " + int(sphereY) + "Z: " + int(sphereZ),300,150);
    		if(inRange>0)
				inRange--;
			return false;
		}
	}

}