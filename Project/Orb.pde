public class Orb {
	int orbType;
	int radius;
	PVector orb;
	float orbSpeed;

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public Orb(){
		orb = new PVector();
		// radius = radius; //In memoriam of Null
		radius = 45;
		randomizeOrb();
		orbSpeed = multiplier;

	}

	public Orb (int zPos) {
		this();
		orb.z = zPos;
		// this.boundingBox = boundingBox;
	}

	void randomizeOrb(){
		orbType = int(random(-1,orbColor.length));
		if(orbType<0)
			orbType = 0;
		orb.y=random(boundingBox.minY+radius/2, boundingBox.maxY-radius/2);
		orb.x=random(boundingBox.minX+radius/2, boundingBox.maxX-radius/2);
	}

	// public Orb (int orbType, int radius, float x, float y, float useX, float useY) {
	// 	// this(orbType,radius);
	// 	// sphereX = x;
	// 	// sphereY = y;
	// 	// this.useX = useX;
	// 	// this.useY = useY;
	// }
	// public Orb (int orbType, int radius, float x, float y) {
	// 	// this(orbType,radius);
	// 	// sphereX = x;
	// 	// sphereY = y;
	// 	// this.useX = 0;
	// 	// this.useY = 0;
	// }

	void draw(){
		orb.z+=orbSpeed;
		if(orb.z>350) {
			orb.z = -int(random(500,1000));
			randomizeOrb();
			orbSpeed = int(random(multiplier/1.2, multiplier*2));
		}
		if(orbType>0) {
			pushMatrix();
			lights();
			translate(orb.x, orb.y, orb.z);
			// fill(155);
			fill(orbColor[orbType]);
			// stroke(55);
			stroke(orbColor[orbType]);
			sphere(radius);
			// ellipse(radius,radius,radius,radius);
			popMatrix();
			// println(orb);
		}
	}

	public boolean inOrbVincinity(PVector playerPos) {
		if(dist(playerPos.x, playerPos.y,0, orb.x, orb.y, orb.z) <= radius*2){
			orb.z = 500;
			return true;
		}
		return false;
	}

}