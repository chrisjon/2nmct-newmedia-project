// import com.onformative.leap.*;
import com.neurogami.leaphacking.*;
import com.leapmotion.leap.*;

class PointerListener extends Listener {
  // code goes here 
  void onFrame(Controller controller) {

    Frame frame = controller.frame();
    InteractionBox box = frame.interactionBox();
    HandList hands = frame.hands();
    

    if (hands.count() > 0 ) {
      Hand hand = hands.get(0);
      FingerList fingers = hand.fingers();
      if (fingers.count() > 0) {
        avgPos = Vector.zero();
        for (Finger finger : fingers) {
          avgPos  = avgPos.plus(finger.tipPosition());
        }
        avgPos = avgPos.divide(fingers.count());
        setNormalizedAvgPos(box.normalizePoint(avgPos));
      } 
    } 
  } 
}