public class Star {
	PVector ster = new PVector();
	BoundingBox boundingBox;
	int starRadius = 100, starPicker = 0;

	public Star(BoundingBox boundingBox) {
		this.boundingBox = boundingBox;
		// randomizeStar();
		starPicker = int(random(starImages.length));
	}
	// void loadStars(String starString){
	// 	star = loadShape("Stars/"+starString);
	// 	// star2 = loadShape("Stars/Pollux .svg");
	// 	// star3 = loadShape("Stars/Siruis.svg");
	// }

	PVector getPos(){
		return ster;
	}

	int getRadius(){
		return starRadius;
	}

	// float randomNoise(float min, float max) {
	// 	return (noise(min,max) * (max - min)) + min;
	// }

	void randomizeStar()
	{	
			ster.y = random(boundingBox.minY+starRadius, boundingBox.maxY-starRadius);
			ster.x = random(boundingBox.minX+starRadius, boundingBox.maxX-starRadius);
	}
	void drawStars()
	{
			shape(starImages[starPicker], ster.x, ster.y, starRadius, starRadius);
			// shape(star2, starX.get(i), starY.get(i), 100, 100);
			// shape(star3, starX.get(i), starY.get(i), 100, 100);
	}

	//TODO
	// boolean isnear(Star that) {
	// 	return (dist(star.x,star.y,star.z,that.star.x,that.star.y,that.star.z));
	// }
}