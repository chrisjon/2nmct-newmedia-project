public class GameWall {
	float zPos;
	float transp;
	Boolean btrans = null;
	// BoundingBox boundingBox;
	PVector gap = new PVector();
	int gapRadius = 250;
	int speedModifier;
	Star[] stars;
	PShape star, star2, star3;
	FloatList starX = new FloatList();
	FloatList starY = new FloatList();
	int aantalsterren = 25;

	public void setTransparency(boolean yep) {
		btrans = yep;
	}
	public void setTransparency(int val) {
		transp = val;
		btrans = null;
	}

	public GameWall (float zPos, BoundingBox boundingBox) {
		star = loadShape("Stars/Bellatrix.svg");
		star2 = loadShape("Stars/Pollux.svg");
		star3 = loadShape("Stars/Siruis.svg");
		this.zPos = zPos;
		transp = 155;
		btrans = false;
		// this.boundingBox = boundingBox;
		stars = createStars(20,boundingBox);
		randomizeGap();
		speedModifier = 1;
		// for (int i = 0; i<aantalsterren;i++)
		// 	randomizeStars();
	}

	public GameWall () {
		// this(0);
	}

	void draw(float speed, float width, float height, float offW, float offH){

		zPos += speed * speedModifier;
		updateTransparency();
		pushMatrix();
		translate(0,0,zPos);
		// fill(55,transp);
		noFill();
		beginShape(QUADS);
		vertex(offW, offH, 0);
		vertex(offW, height-offH, 0);
		vertex(width-offW, height-offH, 0);
		vertex(width-offW, offH, 0);
		endShape();
		for (Star star : stars) {
			star.drawStars();
		}
		drawCircle();
		popMatrix();
	}
	
	void randomizeGap(){
		gap.y=random(boundingBox.minY+gapRadius/2, boundingBox.maxY-gapRadius/2);
		gap.x=random(boundingBox.minX+gapRadius/2, boundingBox.maxX-gapRadius/2);
		for (Star star : stars) {
			do{
				star.randomizeStar();
			} while (isNearGap(star.getPos(),star.getRadius()));
		}

	}

	void drawCircle(){
		// float max = height-offH;
		// float min = offH;
		// float minL = offW;
		// float maxL = width-offW;
		fill(0, 0, 0, transp);
		ellipse(gap.x, gap.y, gapRadius, gapRadius);
	}

	void updateTransparency() {
		if(btrans==null)
			println("btrans: "+btrans);
		else if(btrans) {
			if(transp>0)
				transp-=5;
			else
				zPos = -500;
		} else {
			if(transp<255) 
				transp+=5;
		}

	}

	public void toBack() {
		zPos = -500;
		btrans = false;
		transp = 155;
		randomizeGap();
		speedModifier = 1;
	}

	public void passThrough() {
      if(!onTouchGameWall(translation)){
        println("collission detected, TODO: lose life");
        if(invincibilityCounter==0)
        	lives--;
        if(lives==0) {
        	toBack();
        	gameState = GAME_OVER;
        }
        println("lives: "+lives);
      }
      setTransparency(true);
      speedModifier = 10;
	}
	private boolean onTouchGameWall(PVector playerPos) {
		return (dist(playerPos.x, playerPos.y, gap.x, gap.y) <= gapRadius/2-playerSize);
	}
	public boolean isNearGap(PVector pos, int extRad) {
		// return (dist(pos.x, pos.y, gap.x, gap.y) <= (gapRadius+extRad)*2);
		return false;
	}

	public final Star[] createStars(int amount, BoundingBox boundingBox) {
		Star[] stars = new Star[amount];
		for (int i = 0; i < amount; ++i) {
			stars[i] = new Star(boundingBox);
		}
		return stars;
	}
}